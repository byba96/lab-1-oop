#include "stdafx.h"
#include "CppUnitTest.h"
#include "../lab1OOP/container_atd.h"
#include "../lab1OOP/shape_atd.h"
#include "../lab1OOP/circle_atd.h"
#include "../lab1OOP/rectangle_atd.h"
#include "../lab1OOP/triangle_atd.h"
#include <iostream>
#include <fstream>
#include <string>


using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace simple_shapes;

namespace SortTest
 {
	TEST_CLASS(SortTest)
		 {
		public:
			
				TEST_METHOD(Sort)
				{
				container *c = new container();
				shape *r1 = new rectangle(0, 2, 0, 2, shape::color::RED, 1);
				shape *c1 = new circle(0, 0, 1, shape::color::RED, 1);
				shape *tr1 = new triangle(0, 0, 0, 3, 4, 0, shape::color::RED, 1);
				
				c->AddShape(r1);
				c->AddShape(c1);
				c->AddShape(tr1);
				
				container *SortC = new container();
				SortC->AddShape(tr1);
				SortC->AddShape(r1);
				SortC->AddShape(c1);
	
				c->Sort();

				ofstream ofstc("ContTest.txt");
				ofstream ofstSort("SortTest.txt");
				c->Out(ofstc);
				SortC->Out(ofstSort);
				
				ifstream ifstc("ContTest.txt");
				ifstream ifstSort("SortTest.txt");
				string s;
				string exp;
				while (!ifstSort.eof())
				{
					getline(ifstSort, exp);
					getline(ifstc, s);
					Assert::AreEqual(exp, s);
				}
				}

				};
	}