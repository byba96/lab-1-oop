#include "stdafx.h"
#include "CppUnitTest.h"
#include "../lab1OOP/container_atd.h"
#include "../lab1OOP/shape_atd.h"
#include "../lab1OOP/circle_atd.h"
#include "../lab1OOP/rectangle_atd.h"
#include "../lab1OOP/triangle_atd.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace simple_shapes;

namespace OutDataTest
 {
	TEST_CLASS(OutDataTest)
		 {
		public:
			
				TEST_METHOD(OutEmptyCont)
				 {
				ofstream ofst("EmptyOutTest.txt");
				container expected;
				expected.Out(ofst);
				
				ifstream ofs("EmptyOutTest.txt");
				ifstream ifs("EmptyInTest.txt");
				string s;
				string exp;
				while (!ifs.eof())
					 {
					getline(ifs, exp);
					getline(ofs, s);
					Assert::AreEqual(exp, s);
					}
				}
			
				TEST_METHOD(OutFullCont)
				 {
				ofstream ofst("OutTest.txt");
				
				container expected;
				shape *tr1 = new triangle(0, 0, 1, 1, 2, 2, shape::color::YELLOW, 6.0);
				shape *c1 = new circle(0, 0, 1, shape::color::ORANGE, 5.1);
				shape *r1 = new rectangle(0, 0, 2, 2, shape::color::RED, 1.3);
				expected.AddShape(tr1);
				expected.AddShape(r1);
				expected.AddShape(c1);
				expected.Out(ofst);
				
				ifstream ofs("OutTest.txt");
				ifstream ifs("InTest.txt");
				string s;
				string exp;
				while (!ifs.eof())
					 {
					getline(ifs, exp);
					getline(ofs, s);
					Assert::AreEqual(exp, s);
					}	
				}			
			};
	}