#include "stdafx.h"
#include "CppUnitTest.h"
#include "../lab1OOP/container_atd.h"
#include "../lab1OOP/shape_atd.h"
#include "../lab1OOP/circle_atd.h"
#include "../lab1OOP/rectangle_atd.h"
#include "../lab1OOP/triangle_atd.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace simple_shapes;

namespace InDataTest
{
	TEST_CLASS(InDataTest)
	{
	public:
		
		TEST_METHOD(InEmptyFile)
		{
			ifstream ifst("EmptyFile.txt");
			container c;
			c.In(ifst);
			Assert::AreEqual(c.len, 0);
		}

		TEST_METHOD(InFullFile)
		{
			ifstream ifst("FullFile.txt");
			container c;
			c.In(ifst);

			container expected;
			shape *r1 = new rectangle(0, 2, 0, 2, shape::color::RED, 1.3);
			shape *c1 = new circle(0, 0, 1, shape::color::RED, 5.1);
			shape *tr1 = new triangle(0, 0, 1, 1, 2, 2, shape::color::RED, 6.0);
			expected.AddShape(r1);
			expected.AddShape(c1);
			expected.AddShape(tr1);

			ofstream ofstc("DataInTest.txt");
			ofstream ofste("DataInTest2.txt");
			c.Out(ofstc);
			expected.Out(ofste);

			ifstream ifstc("DataInTest.txt");
			ifstream ifstSort("DataInTest2.txt");
			string s;
			string exp;
			while (!ifstSort.eof())
			{
				getline(ifstSort, exp);
				getline(ifstc, s);
				Assert::AreEqual(exp, s);
			}
		}

		TEST_METHOD(InNonexistentFile)
		{
			ifstream ifst("C://in.txt");
			container c;
			c.In(ifst);
			Assert::AreEqual(c.len, 0);
		}
	};
}