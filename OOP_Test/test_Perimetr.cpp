#include "stdafx.h"
#include "CppUnitTest.h"
#include "../lab1OOP/container_atd.h"
#include "../lab1OOP/shape_atd.h"
#include "../lab1OOP/circle_atd.h"
#include "../lab1OOP/rectangle_atd.h"
#include "../lab1OOP/triangle_atd.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace simple_shapes;

namespace PerimeterTest
 {
	TEST_CLASS(PerimeterTest)
		 {
			public:
		
				TEST_METHOD(PerimeterCircle)
				{
					int r = 2;
				float expected = 2*3.14*r;
				shape *sp = new circle(1, 1, r, shape::color::RED, 1);
				float p = sp->Perimeter();
				Assert::AreEqual(expected, p);
				}
			
				TEST_METHOD(PerimeterRectangle)
				{
				float expected = 0;
				shape *sp = new rectangle(0, 2, 0, 2, shape::color::RED, 1);
				float p = sp->Perimeter();
				Assert::AreEqual(expected, p);
				}
			
				TEST_METHOD(PerimeterTriangle)
				{
				float expected = 12.0;
				shape *sp = new triangle(0, 0, 0, 3, 4, 0, shape::color::RED, 1);
				float p = sp->Perimeter();
				Assert::AreEqual(expected, p);
				}		
				};
	}