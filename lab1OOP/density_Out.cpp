#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"
#include "rectangle_atd.h"
#include "circle_atd.h"
using namespace std;
namespace simple_shapes 
{
	// ����� ���������� ������
	void shape::Out(ofstream &ofst) 
	{
		if (density > 0)
		{
			ofst << "density: " << density;
		}
		else
			ofst << "Check the input data. Density must be greater than zero." << endl;
	}
} // end simple_shapes namespace