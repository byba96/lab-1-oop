#include "stdafx.h"
#include <fstream>
#include "rectangle_atd.h"

using namespace std;

namespace simple_shapes 
{
	void rectangle::Out(ofstream &ofst) 
	{
		if (x1 != y1 && x2 != y2)
		{
			ofst << "It is Rectangle: x1 = " << x1
				<< ", x2 = " << x2
				<< ", y1 = " << y1
				<< ", y2 = " << y2
				<< ", color: ";
			ColorOut(ofst);
			shape::Out(ofst);
			ofst << endl;
		}
		else
			ofst << "Check the input data. The coordinates must be different." << endl;
	}
} // end simple_shapes namespace