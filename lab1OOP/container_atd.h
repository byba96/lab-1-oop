#ifndef __container_atd__
#define __container_atd__
#include "shape_atd.h"
namespace simple_shapes {
	
	// ���������� ��������� �� ������ ����������� �������
	class container 
	{
		enum { max_len = 100 }; // ������������ �����
	public:
		int len; // ������� �����
		shape *cont[max_len];
		void In(ifstream &ifst); // ����
		void Out(ofstream &ofst); // �����
		// ����� ����� � ���������
		void Perimeter(ofstream &ofst);
		void Sort(); // ���������� ����������
		void Clear(); // ������� ���������� �� �����
		void AddShape(shape * sp);
		container(); // ������������� ����������
		~container() { Clear(); } // ���������� ����������
	};
} // end simple_shapes namespace
#endif