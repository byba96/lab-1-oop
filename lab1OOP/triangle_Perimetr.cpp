#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"


using namespace std;

namespace simple_shapes 
{
	float triangle::Perimeter() 
	{
		float a;
		float b;
		float c;
		if ((x1==y1 && y1==z1) || (x2==y2 && y2==z2) || (x1==y1 && x2==y2) || (x1==y1 && x2==y2) ||
			(x1==z1 && x2==z2) || (y1==z1 && y2==z2))
			return 0;
		else
		{
			a = sqrt((y1 - x1)*(y1 - x1) + (y2 - x2)*(y2 - x2));
			b = sqrt((z1 - x1)*(z1 - x1) + (z2 - x2)*(z2 - x2));
			c = sqrt((z1 - y1)*(z1 - y1) + (z2 - y2)*(z2 - y2));
			return a + b + c;
		}
			
	}
} // end simple_shapes namespace