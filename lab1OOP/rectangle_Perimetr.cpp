#include "stdafx.h"
#include <fstream>
#include "rectangle_atd.h"

using namespace std;

namespace simple_shapes 
{
	float rectangle::Perimeter() 
	{
		if (x1 != y1 && x2 != y2)
		{
			return (abs(y2 - x2) + abs(y1 - x1)) * 2;
		}
		else
			return 0;
	}
} // end simple_shapes namespace