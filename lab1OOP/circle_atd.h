#ifndef __circle_atd__
#define __circle_atd__
#include "shape_atd.h"
namespace simple_shapes {
	// ����
	class circle : public shape 
	{
		int x, y, r; // ���������� ������ � ������
	public:
		// �������������� ��������� ������
		void InData(ifstream &ifst); // ����
		void Out(ofstream &ofst); // �����
		float Perimeter(); // ���������� ���������
		circle() {} // �������� ��� �������������.
		circle(int x1, int y1, int r1, color color1, float dens) :
			x(x1), y(y1), r(r1), shape(color1, dens) {};
	};
} // end simple_shapes namespace
#endif