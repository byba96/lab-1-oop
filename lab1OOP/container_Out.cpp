#include "stdafx.h"
#include <fstream>
#include "container_atd.h"

using namespace std;
namespace simple_shapes 
{
	// Вывод содержимого контейнера
	void container::Out(ofstream &ofst) 
	{
		Sort();
		ofst << "Container contents " << len
			<< " elements." << endl;
		for (int i = 0; i < len; i++) 
		{
			ofst << i << ": ";
			cont[i]->Out(ofst);
			if (cont[i]->Perimeter()>0)
			{
				ofst << "perimeter = "<< cont[i]->Perimeter() << endl;
			}
			else
				ofst << "Check the input data. Perimeter must be greater than zero." << endl;

		
		}
	}
} // end simple_shapes namespace