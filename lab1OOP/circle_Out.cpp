#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"


using namespace std;

namespace simple_shapes 
{
	void circle::Out(ofstream &ofst)
	{
		if (r > 0)
		{
			ofst << "It is Circle: x = "
				<< x << ", y = " << y
				<< ", r = " << r << ", color: ";
			ColorOut(ofst);
			shape::Out(ofst);
			ofst << endl;
		}
		else
			ofst << "Check the input data. The radius must be greater than zero." << endl;
	}
} // end simple_shapes namespace