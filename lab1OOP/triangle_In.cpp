#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"


using namespace std;

namespace simple_shapes 
{
	void triangle::InData(ifstream &ifst)
	{
		int c;
		ifst >> x1 >> x2 >> y1 >> y2 >> z1 >> z2 >> c;
		ColorIn(c);
		shape::InData(ifst);
	}
} // end simple_shapes namespace