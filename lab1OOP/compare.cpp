#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	bool shape::Compare(shape &other) 
	{
		return Perimeter() < other.Perimeter();
	}
} // end simple_shapes namespace