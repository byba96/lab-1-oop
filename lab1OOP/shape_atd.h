#ifndef __shape_atd__
#define __shape_atd__
#include <fstream>
#include <string>
using namespace std;
namespace simple_shapes 
{
	class shape// ���������, ���������� ��� ��������� ������
	 {
		protected:
			void ColorIn(int colorNumber);
			void ColorOut(ofstream &ofst);
			float density;
		public:
			enum color { RED, ORANGE, YELLOW, GREEN, BLUE, DARKBLUE, PURPLE };
			color c;
			shape() {};
			shape(color color1, float dens) :c(color1), density(dens) {};
			static shape* In(ifstream &ifst);
			virtual void InData(ifstream &ifst) = 0; 
			virtual void Out(ofstream &ofst) = 0;
			virtual float Perimeter() = 0; // ���������� ���������	
			bool Compare(shape &other);// ��������� ���� ��������		
			virtual void OutRect(ofstream &ofst);// ����� ������ ���������������
		};
} // end simple_shapes namespace
#endif