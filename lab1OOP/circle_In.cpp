#include "stdafx.h"
#include <fstream>
#include "circle_atd.h"


using namespace std;

namespace simple_shapes 
{
	void circle::InData(ifstream &ifst)
	{
		int c;
		ifst >> x >> y >> r>>c;
		ColorIn(c);
		shape::InData(ifst);
	}
} // end simple_shapes namespace