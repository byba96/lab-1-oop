#include "stdafx.h"
#include <fstream>
#include "triangle_atd.h"


using namespace std;

namespace simple_shapes
{
	void triangle::Out(ofstream &ofst)
	{
		if (y1 != x1 && y2 != x2 && z1 != x1 && z2 != x2 && z1 != y1 && z2 != y2)
		{
			ofst << "It is Triangle: x1 = "
				<< x1 << ", x2 = " << x2
				<< ", y1 = " << y1
				<< ", y2 = " << y2
				<< ", z1 = " << z1
				<< ", z2 = " << z2
				<< ", color: ";
			ColorOut(ofst);
			shape::Out(ofst);
			ofst << endl;
		}
		else
			ofst << "Check the input data. The coordinates of triangle different." << endl;
	}
} // end simple_shapes namespace