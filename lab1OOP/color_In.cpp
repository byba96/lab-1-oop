#include "stdafx.h"
#include <fstream>
#include "shape_atd.h"

using namespace std;

namespace simple_shapes 
{
	void shape::ColorIn(int colorNumber)
	{
		if (colorNumber == 1)
		{
			c = shape::color::RED;
		}
		if (colorNumber == 2)
		{
			c = shape::color::ORANGE;
		}		
		if (colorNumber == 3)
		{
			c = shape::color::YELLOW;
		}
		if (colorNumber == 4)
		{
			c = shape::color::GREEN;
		}
		if (colorNumber == 5)
		{
			c = shape::color::BLUE;
		}
		if (colorNumber == 6)
		{
			c = shape::color::PURPLE;
		}	
	}
}