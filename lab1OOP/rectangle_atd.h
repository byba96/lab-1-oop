#ifndef __rectangle_atd__
#define __rectangle_atd__
#include "shape_atd.h"
namespace simple_shapes {
	// �������������
	class rectangle : public shape 
	{
		int x1, x2, y1, y2 ; // ���������� ������ �������� � ������� ������� ����� ��������������
	public:
		// �������������� ��������� ������
		void InData(ifstream &ifst); // ����
		void Out(ofstream &ofst); // �����
		float Perimeter(); // ���������� ���������
		// ����� ������ ���������������
		void OutRect(ofstream &ofst);
		rectangle() {} // �������� ��� �������������.
		rectangle(int x11, int x22, int y11, int y22, color color1, float dens) :
			x1(x11), x2(x22), y1(y11), y2(y22), shape(color1, dens) {};
	};
} // end simple_shape snamespace
#endif